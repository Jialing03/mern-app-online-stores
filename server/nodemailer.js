const nodemailer = require("nodemailer");
require('dotenv').config();

const {
    EMAIL_HOST,
    EMAIL_PORT,
    EMAIL_USERNAME,
    EMAIL_PW
} = process.env;

const emailService = nodemailer.createTransport({
    host: EMAIL_HOST,
    port: EMAIL_PORT,
    secure: false,
    tls: {
       ciphers:'SSLv3'
    },
    auth: {
        user: EMAIL_USERNAME,
        pass: EMAIL_PW,
    },
});

module.exports = emailService;