var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ItemSchema = new Schema({
    title: { type: String, required: true },
    price: { type: String, required: true },
    quantity: { type: Number, required: true },
    seller: {type: String, required: true},
    description: { type: String },
    image: { type: String}
});

module.exports = mongoose.model('Item', ItemSchema);