var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CartSchema = new Schema({
    username: { type: String, required: true, index: {unique: true, dropDups: true} },
    items: { type: Object, required: true },
});
const Cart = mongoose.model('Cart', CartSchema);
Cart.createIndexes();
module.exports = Cart;