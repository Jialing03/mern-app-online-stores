var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OrderSchema = new Schema({
    username: { type: String, required: true},
    items: { type: Object, required: true },
    payment: { type: Object, required: true },
    total: {type: Number, required: true},
    statusHistory: {type: Array, required: true}
});
const Order = mongoose.model('Order', OrderSchema);

module.exports = Order;