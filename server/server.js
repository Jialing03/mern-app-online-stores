const express = require('express');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const randomstring = require("randomstring");
const ejwt = require('express-jwt');
const User = require('./models/user');
const Item = require('./models/item');
const Cart = require('./models/cart');
const Order = require('./models/order');
const VerifyCode = require('./models/verifyCode');
const emailService = require('./nodemailer');

// Envionment variables
require('dotenv').config();
const { PORT, SECRET } = process.env;

// DB Configuration
require('./database');

const app = express();
app.use(cors(), express.json());

app.post('/signup', async (req, res) => {
    const { role, username, password } = req.body;
    // create a user a new user
    const newUser = new User({
        username: username,
        password: password,
        role: role
    });

    try {
        const user = await newUser.save();
        const randomCode = randomstring.generate(12);
        const verifyCode = new VerifyCode({
            email: user.username,
            code: randomCode
        });
        await verifyCode.save();
        const baseUrl = req.protocol + "://" + req.get("host");
        const { EMAIL_USERNAME } = process.env;
        const mail = {
            from: EMAIL_USERNAME,
            to: user.username,
            subject: "Your Activation Link for MERN STORES",
            text: `Please use the following link within the next 10 minutes to activate your account on MERN STORES: ${baseUrl}/verification/${user._id}/${randomCode}`,
            html: `<p>Please use the following link within the next 10 minutes to activate your account on MERN STORES: 
                    <strong><a href="${baseUrl}/verification/${user._id}/${randomCode}" target="_blank">Email Verification</a></strong>
                   </p>`,
        };
        await emailService.sendMail(mail);
        res.sendStatus(200);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

app.get('/verification/:userId/:verifyCode', async (req, res) => {
    try {
        const user = await User.findById(req.params.userId);
        const response = await VerifyCode.findOne({
            email: user.username,
            code: req.params.verifyCode,
        }).exec();
        if (!response) {
            res.sendStatus(401);
        } else {
            await User.updateOne(
                { username: user.username },
                { status: "active" }
            );
            await VerifyCode.deleteMany({ email: user.username });
            res.redirect(`http://127.0.0.1:3000/profile`)
        }
    } catch (err) {
        console.log("Error on /verification: ", err);
        res.sendStatus(500);
    }

})

app.post('/login', (req, res) => {
    const { username, password } = req.body;
    User.findOne({ username: username, status: 'active' }, function (err, user) {
        if (err) throw err;
        if (user === null) {
            res.status(401).send({ errorMsg: 'non verified username' });
        } else {
            // test a matching password
            user.comparePassword(password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    tokenString = jwt.sign({ username: user.username, role: user.role }, SECRET)
                    res.send({ token: tokenString });
                } else {
                    res.status(401).send({ errorMsg: 'wrong password' });
                }
            });
        }
    });
});

app.get('/items', async (req, res) => {
    const { queryString, currentPage } = req.query;
    const itemsPerPage = 5;
    let query = {};
    if (queryString) {
        query = {
            $or: [{ title: { $regex: `.*${queryString}.*`, $options: 'i' } },
            { description: { $regex: `.*${queryString}.*`, $options: 'i' } }]
        };
    }
    const count = await Item.countDocuments(query);
    const items = await Item.find(query, [], { skip: itemsPerPage * (currentPage - 1), limit: itemsPerPage });
    res.send({ items: items, itemCount: count });
});

app.get('/item', async (req, res) => {
    const { id } = req.query;
    res.send(await Item.findOne({ _id: id }));
});

app.get('/cart', ejwt({ secret: SECRET, algorithms: ['HS256'] }), async (req, res) => {
    const cart = await Cart.findOne({ username: req.user.username });
    res.send(cart ? cart.items : []);
});

app.post('/addToCart', ejwt({ secret: SECRET, algorithms: ['HS256'] }), async (req, res) => {
    const cart = await Cart.findOne({ username: req.user.username });
    const { _id, quantity } = req.body;
    let item = await Item.findOne({ _id: _id });
    if (!item || quantity > item.quantity) {
        res.status(500).send({ errorMsg: 'Item is not available anymore.' });
    } else {
        item.quantity = quantity;
        if (!cart) {
            let newCart = { username: req.user.username, items: {} };
            newCart.items[_id] = item;
            Cart.create(newCart);
        } else {
            if (cart.items.hasOwnProperty(_id)) {
                cart.items[_id].quantity += quantity;
            } else {
                cart.items[_id] = item;
            }
            cart.markModified('items');
            await cart.save();
        }
        res.sendStatus(200);
    }
});

app.post('/removeFromCart', ejwt({ secret: SECRET, algorithms: ['HS256'] }), async (req, res) => {
    const cart = await Cart.findOne({ username: req.user.username });
    const { _id, quantity } = req.body;
    if (!cart || !cart.items.hasOwnProperty(_id)) {
        res.status(500).send({ errorMsg: 'Item is not in your cart anymore.' });
    } else {
        if (cart.items[_id].quantity <= quantity) {
            delete cart.items[_id];
        } else {
            cart.items[_id].quantity -= quantity;
        }

        if (Object.keys(cart.items).length === 0) {
            await cart.delete();
        } else {
            cart.markModified('items');
            await cart.save();
        }
        res.sendStatus(200);
    }
});

const verifyPayment = (payment)=> {
    return payment.card.startsWith("8888");
}

app.post('/checkOut', ejwt({ secret: SECRET, algorithms: ['HS256'] }), async (req, res) => {
    const user = await User.findOne({ username: req.user.username });
    const { items, total, payment, orderSummary } = req.body;
    if (!verifyPayment(payment)){
        res.status(500).send({ errorMsg: 'Invalid Credit Card!' });
    } else {
        // create order
        const order = await Order.create({username: user.username, items: items, total: total, payment: payment, statusHistory: [{time: new Date(), status: "pending"}]});
        // empty shopping cart
        await Cart.deleteOne({ username: req.user.username });
        // update inventory quantity
        user.Address = payment.Address;
        await user.save();
        items.forEach(async item=>{
            const stockItem = await Item.findById(item._id);
            if (stockItem.quantity <= item.quantity) {
                await Item.deleteOne({_id: stockItem._id});
            } else {
                stockItem.quantity -= item.quantity;
                await stockItem.save();
            }
        });
        // send confirm email
        const { EMAIL_USERNAME } = process.env;
        const mail = {
            from: EMAIL_USERNAME,
            to: req.user.username,
            subject: `Your Order #${order._id} has been received`,
            text: orderSummary,
            html: orderSummary,
        };
        await emailService.sendMail(mail);
        res.sendStatus(200);
    }
});

app.get('/orders', ejwt({ secret: SECRET, algorithms: ['HS256'] }), async (req, res) => {
    const user = await User.findOne({ username: req.user.username });
    res.send(await Order.find({ username: user.username }));
});

app.get('/orderStatus', ejwt({ secret: SECRET, algorithms: ['HS256'] }), async (req, res) => {
    const user = await User.findOne({ username: req.user.username });
    const { orderID } = req.query;
    res.send(await Order.findOne({ _id: orderID, username: user.username }));
});

app.listen(PORT, function () {
    console.log(`Server Listening on ${PORT}`);
});