import React, { useEffect, useState } from 'react';
import { renderToString } from 'react-dom/server'
import { Button, Row, Col, Container, Table, Form } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import Login from '../common/login';
import { fetchCartItems, checkOutItems } from '../common/api';
import { RightButton } from './cart';
import OrderSummary from './orderSummary';

const states = ["Alaska",
                  "Alabama",
                  "Arkansas",
                  "American Samoa",
                  "Arizona",
                  "California",
                  "Colorado",
                  "Connecticut",
                  "District of Columbia",
                  "Delaware",
                  "Florida",
                  "Georgia",
                  "Guam",
                  "Hawaii",
                  "Iowa",
                  "Idaho",
                  "Illinois",
                  "Indiana",
                  "Kansas",
                  "Kentucky",
                  "Louisiana",
                  "Massachusetts",
                  "Maryland",
                  "Maine",
                  "Michigan",
                  "Minnesota",
                  "Missouri",
                  "Mississippi",
                  "Montana",
                  "North Carolina",
                  " North Dakota",
                  "Nebraska",
                  "New Hampshire",
                  "New Jersey",
                  "New Mexico",
                  "Nevada",
                  "New York",
                  "Ohio",
                  "Oklahoma",
                  "Oregon",
                  "Pennsylvania",
                  "Puerto Rico",
                  "Rhode Island",
                  "South Carolina",
                  "South Dakota",
                  "Tennessee",
                  "Texas",
                  "Utah",
                  "Virginia",
                  "Virgin Islands",
                  "Vermont",
                  "Washington",
                  "Wisconsin",
                  "West Virginia",
                  "Wyoming"]
const months = ["1",
               "2",
               "3",
               "4",
               "5",
               "6",
               "7",
               "8",
               "9",
               "10",
               "11",
               "12"]
const years = ["2022",
               "2023",
               "2024",
               "2025",
               "2026"]

export default function Checkout({ setToken, token }) {
    const [cartItems, setCartItems] = useState([]);
    const [totalPrice, setTotal] = useState(0);
    const [card, setCard] = useState("");
    const [month, setMonth] = useState("");
    const [year, setYear] = useState("");
    const [code, setCode] = useState("");
    const [firstname, setFirstName] = useState("");
    const [lastname, setLastName] = useState("");
    const [address1, setAddress1] = useState("");
    const [address2, setAddress2] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [zip, setZip] = useState("");
    const [error, setError] = useState();

    let history = useHistory();

    const refresh = async () => {
        setCartItems(await fetchCartItems(token));
    }

    useEffect(async () => {
        await refresh();
    }, []);

    useEffect(() => setTotal(cartItems.reduce((r, item) => r + item.total, 0)), [cartItems]);

    const checkOut = async event => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.stopPropagation();
        }
        const payment = {
            card: card,
            month: month, 
            year: year,
            code: code, 
            firstname: firstname,
            lastname: lastname,
            address1: address1, 
            address2: address2, 
            city: city, 
            state: state, 
            zip: zip
        };
        const orderSummary = renderToString(<OrderSummary cartItems={cartItems} totalPrice={totalPrice}/>);
        const res = await checkOutItems({ token, cartItems, totalPrice, payment, orderSummary });
        if (res === 'OK') {
            history.push({ pathname: "/orderConfirmation", state: {cartItems: cartItems, totalPrice: totalPrice}});
        } else {
            setError(res.errorMsg);
            console.log(res);
        }
    }

    if (!token) {
        return (
            <div>
                <h3>To checkout your cart, please login first.</h3>
                <Login setToken={setToken} />
            </div>
        );
    }

    return (<Container>
        {cartItems.length === 0 ?
            <a href='/'>Your Cart is empty. Go to find something interesting.</a> :
            <OrderSummary cartItems={cartItems} totalPrice={totalPrice}/>
        }
        <Form onSubmit={checkOut}>
            <Form.Row>
                <Form.Group as={Col} controlId="card">
                    <Form.Label>Card Number</Form.Label>
                    <Form.Control required onChange={e => setCard(e.target.value)} isInvalid={error !== null}/>
                    <span style={{ color: 'red' }}>{error}</span>
                </Form.Group>

                <Form.Group as={Col} controlId="Exp">
                    <Form.Label>Expiration</Form.Label>
                    <Row>
                        <Col><Form.Control as="select" required defaultValue="Choose..." onChange={e => setMonth(e.target.value)}>
                            {months.map((month, _) => <option key={month}>{month}</option>)}
                        </Form.Control></Col>
                        <Col><Form.Control as="select" required defaultValue="Choose..." onChange={e => setYear(e.target.value)}>
                            {years.map((year, _) => <option key={year}>{year}</option>)}
                        </Form.Control></Col>
                    </Row>
                </Form.Group>

                <Form.Group as={Col} controlId="code">
                    <Form.Label>CVV</Form.Label>
                    <Form.Control required onChange={e => setCode(e.target.value)}/>
                </Form.Group>
            </Form.Row>

            <Form.Row>
                <Form.Group as={Col} controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control required placeholder="First Name" onChange={e => setFirstName(e.target.value)}/>
                </Form.Group>
                <Form.Group as={Col} controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control required placeholder="Last Name" onChange={e => setLastName(e.target.value)}/>
                </Form.Group>
            </Form.Row>   
            <Form.Group controlId="formGridAddress1">
                <Form.Label>Address</Form.Label>
                <Form.Control required placeholder="1234 Main St" onChange={e => setAddress1(e.target.value)}/>
            </Form.Group>

            <Form.Group controlId="formGridAddress2">
                <Form.Label>Address 2</Form.Label>
                <Form.Control placeholder="Apartment, studio, or floor" onChange={e => setAddress2(e.target.value)}/>
            </Form.Group>

            <Form.Row>
                <Form.Group as={Col} controlId="formGridCity">
                    <Form.Label>City</Form.Label>
                    <Form.Control required onChange={e => setCity(e.target.value)}/>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>State</Form.Label>
                    <Form.Control as="select" required defaultValue="Choose..." onChange={e => setState(e.target.value)}>
                    {states.map((state, _)=><option key={state}>{state}</option>)}
                        
                    </Form.Control>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridZip">
                    <Form.Label>Zip</Form.Label>
                    <Form.Control required onChange={e => setZip(e.target.value)}/>
                </Form.Group>
            </Form.Row>
            <Row>
                <Col><Link to="/cart"><Button>Back to Cart</Button></Link ></Col>
                <Col><RightButton type="submit">Submit Order</RightButton></Col>
            </Row>
        </Form>
        
    </Container>);
}