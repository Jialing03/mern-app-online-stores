import React, { useEffect, useState } from 'react';
import { ListGroup, Button, Row, Col, Container, Table } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import Login from '../common/login';
import { fetchOrders } from '../common/api';

export const RightButton = styled(Button)`
    float: right;
`;

export default function Order({ setToken, token }) {
    const [orders, setOrders] = useState([]);

    const refresh = async () => {
        setOrders(await fetchOrders(token));
    }

    useEffect(async () => {
        await refresh();
    }, []);

    if (!token) {
        return (
            <div>
                <h3>To see your orders, please login first.</h3>
                <Login setToken={setToken} />
            </div>
        );
    }

    return (<Container>
        {orders.length === 0 ?
            <a href='/'>You have no order so far. Go to find something interesting.</a> :
            
            <Table variant="flush">
            <thead>
                <tr>
                    <th>Order Placed</th>
                    <th>Order ID</th>
                    <th>Items</th>
                    <th>Total</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {orders.map((order, _) =>
                    <tr key={order._id}>
                        <td>{order.date}</td>
                        <td><a href={`/orderStatus/${order._id}`}>{order._id}</a></td>
                        <td>
                            <ListGroup variant="flush">
                                {order.items.map((item, _) => <ListGroup.Item key={item._id}>{item.title}<br/>{item.price} * {item.quantity}</ListGroup.Item>)}
                            </ListGroup></td>
                        <td>${order.total}</td>
                        <td>{order.lastStatus}</td>
                    </tr>)}
            </tbody>
        </Table >
        }
    </Container>);
}