import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ListGroup, Card, Row, Col, Form, Button } from 'react-bootstrap';
import { addToCart, removeFromCart } from '../common/api';
import styled from 'styled-components';

const ClickableTitle = styled(Card.Title)`
    cursor: pointer;
    &:hover {
        color: blue;
        text-decoration: underline;
    }
`;

export default function CartItem({ token, item, refresh }) {
    const { _id, image, title, description, price, quantity } = item;
    const [newQty, setNewQty] = useState(quantity);
    const history = useHistory();

    const updateQty = async () => {
        if (newQty > quantity) {
            await addToCart(token, _id, newQty - quantity)
        } else if (newQty < quantity) {
            await removeFromCart(token, _id, quantity - newQty)
        }
        refresh();
    }

    return (
        <ListGroup.Item>
            <Card border="light">
                <Card.Body>
                    <Row>
                        <Col><Card.Img variant="top" src={image} alt="broken image" /></Col>
                        <Col xs={8}>
                            <ClickableTitle onClick={() => history.push({ pathname: "/item", state: { item_id: _id } })}>{title}</ClickableTitle>
                            <Card.Text>
                                {description ? description.substring(0, 128) + '...' : null}<br /><br />
                                Price: {price}<br /><br />
                            </Card.Text>
                            <Form>
                                <Form.Row className="align-items-center">
                                    <Col xs="auto">
                                        <Form.Label >Quantity:</Form.Label>
                                    </Col>
                                    <Col xs="2">
                                        <Form.Control type="number" size="sm" min={0} value={newQty} onChange={e => setNewQty(e.target.value)} />
                                    </Col>
                                    <Col>
                                        <Button size="sm" onClick={updateQty}>Update</Button>
                                    </Col>
                                </Form.Row>
                            </Form>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </ListGroup.Item>
    );
}