import { useState, useEffect } from "react";
import { Container, ListGroup } from "react-bootstrap";
import { useParams } from 'react-router-dom';
import OrderSummary from "./orderSummary";
import { fetchOrderStatus } from "../common/api";

export default function OrderStatus({ token }) {
    const [items, setItems] = useState();
    const [totalPrice, setTotalPrice] = useState();
    const [payment, setPayment] = useState();
    const [statusHistory, setStatusHistory] = useState([]);
    const { orderID } = useParams();

    useEffect(async () => {
        const { items, total, payment, statusHistory } = await fetchOrderStatus(token, orderID);
        setItems(items);
        setTotalPrice(total);
        setPayment(payment)
        setStatusHistory(statusHistory);
    }, []);
    return (<>{(!items || !payment) ?
        <p>no items</p> :
        <Container>
            <OrderSummary cartItems={items} totalPrice={totalPrice}/>
            <h3>Payment Details</h3>
            <ListGroup variant="flush">
                <ListGroup.Item >Card number: {payment.card}</ListGroup.Item>
                <ListGroup.Item >Name: {payment.firstname} {payment.lastname}</ListGroup.Item>
                <ListGroup.Item >Address: {payment.address1} {payment.city}, {payment.state} {payment.zip}</ListGroup.Item>         
            </ListGroup>
            <h3>Status History</h3>
            <ListGroup variant="flush">
                {statusHistory.map((status, _) => <ListGroup.Item key={status.time}>{status.time}: {status.status}</ListGroup.Item>)}
            </ListGroup>
        </Container>
    }</>);
}