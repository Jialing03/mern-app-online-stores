import React from 'react';
import {Table} from 'react-bootstrap';

export default function OrderSummary({ cartItems, totalPrice }) {
    return (
        <Table variant="flush">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                {cartItems.map((item, _) =>
                    <tr key={item.title}>
                        <td>{item.title}</td>
                        <td>{item.price}</td>
                        <td>{item.quantity}</td>
                        <td>${item.total}</td>
                    </tr>)}
                <tr key="total">
                    <td></td>
                    <td></td>
                    <td>Total:</td>
                    <td>{totalPrice}</td>
                </tr>
            </tbody>
        </Table >
    );
}