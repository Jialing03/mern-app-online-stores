import { useLocation } from "react-router-dom";
import OrderSummary
 from "./orderSummary";
export default function OrderConfirmation() {
    const location = useLocation();
    return (
        <div>
            <h1>Order has been received!</h1>
            <p>Your order details has been sent to email</p>
            <OrderSummary cartItems={location.state?.cartItems} totalPrice={location.state?.totalPrice}/>
        </div>
    );
}