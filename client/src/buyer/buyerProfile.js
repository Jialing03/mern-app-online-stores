import React from 'react';
import { decodeToken } from "react-jwt";
import Login from '../common/login';

export default function BuyerProfile({ setToken, token }) {
//  useEffect()
    if (token) {
        const decodedToken = decodeToken(token);
        return (
            <div>
                <h2>Buyer Profile</h2>
                <p>username: {decodedToken?.username}</p>
                <p>role: {decodedToken?.role}</p>
                {/* <p>address: {} */}
            </div>
        );
    } else {
        return (
            <div>
                <h3>To see your profile, please login first.</h3>
                <Login setToken={setToken} />
            </div>
        );
    }
}