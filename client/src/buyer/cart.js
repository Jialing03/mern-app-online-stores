import React, { useEffect, useState } from 'react';
import { ListGroup, Button, Row, Col, Container } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import Login from '../common/login';
import { fetchCartItems } from '../common/api';
import CartItem from './cartItem';

export const RightButton = styled(Button)`
    float: right;
`;

export default function Cart({ setToken, token }) {
    const [cartItems, setCartItems] = useState([]);

    const refresh = async () => {
        setCartItems(await fetchCartItems(token));
    }

    useEffect(async () => {
        await refresh();
    }, []);

    if (!token) {
        return (
            <div>
                <h3>To see your profile, please login first.</h3>
                <Login setToken={setToken} />
            </div>
        );
    }

    return (<Container>
        <Row>
            <Col><h3>Shopping Cart</h3></Col>
            <Col><Link to="/checkout"><RightButton>Check Out</RightButton></Link ></Col>
        </Row>

        {cartItems.length === 0 ?
            <a href='/'>Your Cart is empty. Go to find something interesting.</a> :
            <ListGroup variant="flush">
                {cartItems.map((item, _) => <CartItem key={item._id} token={token} item={item} refresh={refresh} />)}
            </ListGroup>
        }
    </Container>);
}