import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Button, Form, Container, InputGroup, ListGroup, Card, Row, Col } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import { fetchItems } from './api';

export default function Home() {
    const [items, setItems] = useState([]);
    const [itemCount, setItemCount] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [queryString, setQueryString] = useState();
    let history = useHistory();
    useEffect(async () => {
        const { items, itemCount } = await fetchItems(queryString, currentPage);
        setItems(items);
        setItemCount(itemCount);
    }, [currentPage]);

    const handleQueryStringChange = (event) => {
        setQueryString(event.target.value);
    }

    const handlePageClick = (page) => {
        setCurrentPage(page.selected + 1);
    };

    const searchItems = async () => {
        if (currentPage == 1) {
            const { items, itemCount } = await fetchItems(queryString, currentPage)
            setItems(items);
            setItemCount(itemCount);
        } else {
            setCurrentPage(1);
        }
    }

    return (
        <div>
            <Container>
                <InputGroup className="mb-3">
                    <Form.Control placeholder="Product's name or description" onChange={handleQueryStringChange} />
                    <InputGroup.Append>
                        <Button onClick={searchItems}>Search</Button>
                    </InputGroup.Append>
                </InputGroup>
                <p>{(currentPage - 1) * 5 + 1} - {currentPage * 5} of {itemCount} items</p>
                <ListGroup variant="flush">
                    {items.map((item, index) => {
                        return (
                            <ListGroup.Item key={index}>
                                <Card border="light" onClick={() => history.push({ pathname: "/item", state: { item_id: item._id } })}>
                                    <Card.Body>
                                        <Row>
                                            <Col><Card.Img variant="top" src={item.image} alt="broken image" /></Col>
                                            <Col xs={8}>
                                                <Card.Title>{item.title}</Card.Title>
                                                <Card.Text>
                                                    {item.description ? item.description.substring(0, 384) + '...' : null}<br /><br />
                                                    Price: {item.price}<br /><br />
                                                    Quantity: {item.quantity}</Card.Text>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </ListGroup.Item>
                        );
                    })}
                </ListGroup>
                <ReactPaginate
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    pageCount={itemCount / 5 + 1}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    forcePage={currentPage - 1}
                    breakClassName={'page-item'}
                    breakLinkClassName={'page-link'}
                    containerClassName={'pagination'}
                    pageClassName={'page-item'}
                    pageLinkClassName={'page-link'}
                    previousClassName={'page-item'}
                    previousLinkClassName={'page-link'}
                    nextClassName={'page-item'}
                    nextLinkClassName={'page-link'}
                    activeClassName={'active'}
                />
            </Container>
        </div>
    );
}