import React, { useEffect } from 'react';
import Login from './login';

export default function Logout({ setToken }) {
    useEffect(() => {
        setToken(0);
    }, []);
    return (
        <div>
            <h1>You are logout</h1>
            <Login setToken={setToken} />
        </div>
    );
}