import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Navbar, Nav } from 'react-bootstrap';
import styled from 'styled-components';

import Home from './home';
import Verification from './verification';
import BuyerProfile from '../buyer/buyerProfile';
import Login from './login';
import Logout from './logout';
import Registration from './registration';
import useToken from './useToken';
import Item from './item';
import Cart from '../buyer/cart';
import Checkout from '../buyer/checkout';
import OrderConfirmation from '../buyer/confirmation';
import Order from '../buyer/order';
import OrderStatus from '../buyer/orderStatus';

const StyledNavbar = styled(Navbar)`
    margin-bottom: 1rem;
`;

export default function App() {
    const { token, setToken } = useToken();

    return (
        <BrowserRouter>
            <div>
                <StyledNavbar bg="primary" variant="dark">
                    <Navbar.Brand href="/">Home</Navbar.Brand>
                    <Nav>
                        <Nav.Link href="/profile">Profile</Nav.Link>
                        {token
                            ? <>
                                <Nav.Link href="/order">Order</Nav.Link>
                                <Nav.Link href="/cart">Cart</Nav.Link>
                                <Nav.Link href="/signout">Sign out</Nav.Link>
                            </>
                            : <>
                                <Nav.Link href="/signup">Sign up</Nav.Link>
                                <Nav.Link href="/signin">Sign in</Nav.Link>
                            </>}
                    </Nav>
                </StyledNavbar>

                {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
                <Switch>
                    <Route path="/signin" >
                        <Login setToken={setToken} />
                    </Route>
                    <Route path="/signout">
                        <Logout setToken={setToken} />
                    </Route>
                    <Route path="/signup">
                        <Registration />
                    </Route>
                    <Route path="/verify">
                        <Verification />
                    </Route>
                    <Route path="/profile">
                        <BuyerProfile setToken={setToken} token={token} />
                    </Route>
                    <Route path="/item">
                        <Item setToken={setToken} token={token} />
                    </Route>
                    <Route path="/cart">
                        <Cart setToken={setToken} token={token} />
                    </Route>
                    <Route path="/checkout">
                        <Checkout setToken={setToken} token={token} />
                    </Route>
                    <Route path="/orderConfirmation">
                        <OrderConfirmation />
                    </Route>
                    <Route path="/order">
                        <Order setToken={setToken} token={token} />
                    </Route>
                    <Route path="/orderStatus/:orderID">
                        <OrderStatus setToken={setToken} token={token} />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </div>
        </BrowserRouter>
    );
}