export async function loginUser(credentials) {
    const response = await fetch('http://localhost:8080/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    });
    const content = await response.json();
    if (response.ok) {
        return content;
    } else {
        throw Error(content.errorMsg);
    }
}

export async function registerUser(registrationForm) {
    const response = await fetch('http://localhost:8080/signup', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(registrationForm)
    });
    if (response.ok) {
        return await response.text();
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}

export async function fetchItems(queryString, currentPage) {
    let url = `http://localhost:8080/items?currentPage=${currentPage}`;
    if (queryString) {
        url += `&queryString=${queryString}`;
    }
    const response = await fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (response.ok) {
        return await response.json();
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}

export async function fetchItem(item_id) {
    let url = `http://localhost:8080/item?id=${item_id}`;
    const response = await fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (response.ok) {
        return await response.json();
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}

const extractPrice = (priceStr) =>{
    let matched = priceStr.match(/\$([\d.]+)/);
    if (matched) {
        return Number.parseFloat(matched[1]);
    } else {
        return 0.0;
    }
}

export async function fetchCartItems(token) {
    const response = await fetch('http://localhost:8080/cart', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`
        },
    });
    if (response.ok) {     
        const items = await response.json();
        let itemList = Object.entries(items).map((item) => item[1]);
        itemList.forEach((item) => { 
            item["total"] = Number.parseInt(item.quantity) * extractPrice(item.price); 
        })
        return itemList;
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}

export async function addToCart(token, item_id, quantity) {
    const response = await fetch('http://localhost:8080/addToCart', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({ _id: item_id, quantity: quantity })
    });
    if (response.ok) {
        return await response.text();
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}

export async function removeFromCart(token, item_id, quantity) {
    const response = await fetch('http://localhost:8080/removeFromCart', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({ _id: item_id, quantity: quantity })
    });
    if (response.ok) {
        return await response.text();
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}

export async function checkOutItems({token, cartItems, totalPrice, payment, orderSummary}) {
    const response = await fetch('http://localhost:8080/checkOut', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({items: cartItems, total: totalPrice, payment: payment, orderSummary: orderSummary})
    });
    if (response.ok) {
        return await response.text();
    } else {
        return await response.json();
        // throw Error(await response.body);
    }
}

export async function fetchOrders(token) {
    const response = await fetch('http://localhost:8080/orders', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`
        },
    });
    if (response.ok) {
        const orders = await response.json();
        orders.forEach((order) => {
            const lastStatus = order.statusHistory[order.statusHistory.length - 1];
            order["date"] = lastStatus.time;
            order["lastStatus"] = lastStatus.status;
        })
        return orders;
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}

export async function fetchOrderStatus(token, orderID) {
    const response = await fetch(`http://localhost:8080/orderStatus?orderID=${orderID}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`
        },
    });
    if (response.ok) {     
        return await response.json();
    } else {
        return await response.text();
        // throw Error(await response.body);
    }
}
