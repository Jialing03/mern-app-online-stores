import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Button, Form, Container, Row } from 'react-bootstrap';
import styled from 'styled-components';
import { registerUser } from './api';

const NarrowContainer = styled(Container)`
    width: 24rem;
`;

export default function Registration() {
    const [role, setRole] = useState('buyer');
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [password2, setPassword2] = useState();
    const [pwMatched, setPwMatched] = useState(false);
    const [validated, setValidated] = useState(false);
    const [errorMsg, setErrorMsg] = useState();
    let history = useHistory();

    useEffect(() => {
        setPwMatched(!(password && password2) || password === password2);
        setValidated(password && password2 && password === password2 && username !== undefined);
    });

    const handleSubmit = async e => {
        e.preventDefault();
        const c = {
            role,
            username,
            password
        };
        registerUser(c)
            .then((res) => {
                if (res == 'OK') {
                    history.push({ pathname: "/verify", state: { email: username } });
                } else { // error
                    errorHandling(res);
                }
            })
            .catch(console.log);
    }

    const errorHandling = (res) => {
        setErrorMsg(res);
        // setPassword();
        // setPassword2();
    }

    return (
        <NarrowContainer>
            <Form validated={validated} onSubmit={handleSubmit}>
                <Form.Group>
                    <Form.Label>Account Type</Form.Label>

                    <Form.Control as="select" custom onChange={e => setRole(e.target.value)}>
                        <option>Buyer</option>
                        <option>Seller</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={e => setUserName(e.target.value)} />
                    <span style={{ color: 'red' }}>{errorMsg}</span>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control type="password" placeholder="Confirm Password" onChange={e => setPassword2(e.target.value)} />
                    <span style={{ color: 'red' }}>{pwMatched ? '' : 'not match'}</span>
                </Form.Group>
                <Button type="submit" variant="primary" disable={validated}>
                    Register
                </Button>
            </Form>
        </NarrowContainer>
    );
}