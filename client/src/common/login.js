import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { Button, Form, Container } from 'react-bootstrap';
import { loginUser } from './api';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const NarrowContainer = styled(Container)`
    width: 24rem;
`;

export default function Login({ setToken }) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [errorMsg, setErrorMsg] = useState();
    let role;
    let history = useHistory();

    const handleSubmit = async e => {
        e.preventDefault();
        const c = {
            username,
            password
        };
        loginUser(c)
        .then(token =>  {
            setToken(token);
            history.push({ pathname: "/profile" });
        })
        .catch (err => {
            console.log(err);
            setErrorMsg(err.message);
        });
    }
    
    return (
        <div>
            <NarrowContainer>
                <Form onSubmit={handleSubmit}>
                    <Form.Group>
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" onChange={e => setUserName(e.target.value)} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} />
                    </Form.Group>

                    <Button variant="primary" type="submit" style={{ width: '100%' }}>
                        Sign in
                    </Button>
                </Form>
                <span style={{ color: 'red'}}>{errorMsg}</span>
                <div>
                    <span>Don't have an account?</span> <a href='/signup'>Create one.</a>
                </div>
            </NarrowContainer>
        </div>
    );
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}