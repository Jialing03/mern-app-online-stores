import { useLocation } from "react-router-dom";

export default function Verification() {
    const location = useLocation();
    return (
        <div>
            <h1>Almost done!</h1>
            <p>The verify link has been sent to {location.state?.email}. Please click the link in your email to active your account within 10 minutes.</p>
        </div>
    );
}