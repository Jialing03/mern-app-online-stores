import { useEffect, useState } from 'react';
import { Button, Card, Form, Row } from 'react-bootstrap';
import { useHistory, useLocation } from 'react-router';
import { fetchItem, addToCart } from './api';

export default function Item(token, setToken) {
    const location = useLocation();
    const history = useHistory();
    const [item, setItem] = useState();
    const [quantity, setQuantity] = useState(1);
    const [qtyArray, setqtyArray] = useState([1, 2, 3]);
    useEffect(async () => {
        const item = await fetchItem(location.state?.item_id);
        setItem(item);
        setqtyArray(Array.from({ length: item?.quantity }, (_, i) => i + 1));
    }, []);

    const addItemToCart = async e => {
        e.preventDefault();
        const res = await addToCart(token?.token, item._id, quantity)
        if (res == 'OK') {
            history.push("/cart");
        } else { // error
            console.log(res);
        }
    }
    return (
        <> {item ?
            <Card>
                <Card.Body>
                    <Card.Img variant="top" src={item.image} alt="broken image" style={{width:'30rem'}}/>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text>
                        Price: {item.price}<br /><br />
                        Quantity: {item.quantity}<br /><br />
                        {item.description}
                    </Card.Text>
                    <Form inline onSubmit={addItemToCart}>
                        <Form.Control as="select" onChange={e => setQuantity(parseInt(e.target.value))}>
                            {qtyArray.map((n, i) => <option key={i}>{n}</option>)}
                        </Form.Control>
                        <Button variant="primary" type="submit">Add to cart</Button>
                    </Form>


                </Card.Body>
            </Card>
            :
            <p>Item is not available anymore.</p>
        }</>
    );
}