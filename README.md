# MERN-App-Online-Stores
## dev setup
### run mongodb server at localhost:27017
```console
C:\Program Files\MongoDB\Server\4.4\bin>mongo  "mongodb://localhost:27017"
```

### run express.js back-end at localhost:8080
```console
cd MERN-App-Online-Stores\server
npm install
npm run dev
```
### run reactjs front-end at localhost:3000
```console
cd MERN-App-Online-Stores\client
npm install 
npm start
```
